/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package excepciones;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class EmpNotFoundException extends Exception{
    public EmpNotFoundException(String msg) {
        super(msg);
    }
}
