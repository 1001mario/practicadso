/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import excepciones.BDException;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import modelo.ConfiguracionPC;
import modelo.PC;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class GestorPersistenciaPC {
    
    /**
     * Se obtiene los PCs en stock a partir de
     * @param conf configuracion de un PC
     * @return los PCs en stock
     * @throws BDException 
     */
    public static String[] getPCsEnStock(ConfiguracionPC conf) throws BDException {
        int idConfiguracion = conf.getIdConfiguracion();
        String query = "SELECT * FROM APP.PC WHERE APP.PC.IDCONFIGURACION = "+idConfiguracion+" AND APP.PC.RESERVADO=0";
        ResultSet rs = ConexionBD.getInstancia().ejecutaQuery(query);
        ArrayList<String> pcsStock = new ArrayList<> ();
        try {
            while(rs.next()){
                JsonObject jsonObj=Json.createObjectBuilder()
                .add("etiquetaID",rs.getInt("ETIQUETAID"))
                .add("reservado",rs.getInt("RESERVADO"))
                .add("fechaMontaje",rs.getString("FECHAMONTAJE"))
                .add("montador",rs.getString("MONTADOPOR"))
                .add("idConfiguracion",rs.getInt("IDCONFIGURACION"))
                .add("ubicacion",rs.getInt("UBICACION"))
                .add("idPedido",rs.getInt("IDPEDIDO"))
                .build();
                /*Conversion de Json a String*/
                StringWriter jsonstr=new StringWriter();
                JsonWriter writer = Json.createWriter(jsonstr);
                writer.writeObject(jsonObj);
                pcsStock.add(jsonstr.toString());
                writer.close();
                jsonstr.close();
            }
        } catch(SQLException | IOException e) {
            throw new BDException(e.getMessage());
        }
        String[] jsonPCs = new String[pcsStock.size()];
        jsonPCs = pcsStock.toArray(jsonPCs);
        return jsonPCs;
    }
    /**
     * Se actualiza el PC a reservar a partir de
     * @param idPC 
     */
    public static void reservarPC(int idPC) {
        String query = "UPDATE APP.PC SET APP.PC.RESERVADO=1 WHERE APP.PC.ETIQUETAID= "+idPC;
        try {
            ConexionBD.getInstancia().ejecutaUpdate(query);
        } catch (BDException ex) {
            Logger.getLogger(GestorPersistenciaPC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Se obtiene el PC a partir de 
     * @param etiquetaID
     * @return el pc de la etiquetaID
     * @throws SQLException
     * @throws IOException 
     */
    public static String getPCByEtiquetaID(int etiquetaID) throws SQLException, IOException{
        ResultSet rs;
        String sql = "SELECT * FROM APP.PC WHERE ETIQUETAID ="+etiquetaID +" AND RESERVADO=1";
        
        try{
            /*Lectura de la BD y creación de la cadena Json*/
            rs = ConexionBD.getInstancia().ejecutaQuery(sql);
            if(rs.next()){
                JsonObject jsonObj=Json.createObjectBuilder()
                .add("etiquetaID",rs.getInt("ETIQUETAID"))
                .add("reservado",rs.getInt("RESERVADO"))
                .add("fechaMontaje",rs.getString("FECHAMONTAJE"))
                .add("montador",rs.getString("MONTADOPOR"))
                .add("idConfiguracion",rs.getInt("IDCONFIGURACION"))
                .add("ubicacion",rs.getInt("UBICACION"))
                .add("idPedido",rs.getInt("IDPEDIDO"))
                .build();
                 
                /*Conversion de Json a String*/
                StringWriter jsonstr=new StringWriter();
                JsonWriter writer = Json.createWriter(jsonstr);
                writer.writeObject(jsonObj);
                writer.close();
                jsonstr.close();
                return jsonstr.toString();
            }
        }catch(SQLException e){
            throw new BDException(e.getMessage());
        }
        return null;
    }
    /**
     * Se comprueba si la etiquetaID es correcta
     * @param idPC
     * @return si es valida o no
     * @throws BDException 
     */
    public static boolean idValido (int idPC) throws BDException {
        ResultSet rs;
        String sql = "SELECT * FROM APP.PC WHERE ETIQUETAID ="+idPC;
        
        try{
            /*Lectura de la BD y creación de la cadena Json*/
            rs = ConexionBD.getInstancia().ejecutaQuery(sql);
            if(rs.next()){
                return false;
            }
            return true;
        }catch(SQLException e){
            throw new BDException(e.getMessage());
        }
    }
    /**
     * Se actualiza el PC a registrar   
     * @param pc
     * @return el pc actualizado
     * @throws BDException 
     */
    public static int registrarPC (PC pc) throws BDException {
        int rs;
        String sql = "INSERT INTO APP.PC VALUES("+pc.getEtiquetaId()+", "+pc.getReservado()+", '"+pc.getFechaMontaje()+
                "', "+pc.getConfiguracion().getIdConfiguracion()+", '"+pc.getMontador()+"', "
                +pc.getUbicacion()+", "+pc.getIdPedido();
        
        try{
            /*Lectura de la BD y creación de la cadena Json*/
            rs = ConexionBD.getInstancia().ejecutaUpdate(sql);
            
        }catch(SQLException e){
            throw new BDException(e.getMessage());
        }
        return rs;
    }
}
