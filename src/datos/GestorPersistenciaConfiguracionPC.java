/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import excepciones.BDException;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;
/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class GestorPersistenciaConfiguracionPC {
    /**
     * Se obtiene la configuracion de ConfiguracionPC a partir de
     * @param idConfiguracion
     * @return la configuracion de un PC
     * @throws BDException 
     */
    public static String getConfiguracion (int idConfiguracion) throws BDException {
        String query = "SELECT * FROM APP.CONFIGURACIONPC WHERE IDCONFIGURACION = "+idConfiguracion;
        ResultSet rs = ConexionBD.getInstancia().ejecutaQuery(query);
        try {
            if(rs.next()){
                JsonObject jsonObj=Json.createObjectBuilder()
                .add("idConfiguracion",rs.getString("IDCONFIGURACION"))
                .add("tipoCPU",rs.getString("TIPOCPU"))
                .add("velocidadCPU",rs.getString("VELOCIDADCPU"))
                .add("capacidadRAM",rs.getString("CAPACIDADRAM"))
                .add("capacidadDD",rs.getString("CAPACIDADDD"))
                .add("velocidadTarjetaGrafica",rs.getString("VELOCIDADTARJETAGRAFICA"))
                .add("memoriaTarjetaGrafica",rs.getString("MEMORIATARJETAGRAFICA"))
                .build();
                
                /*Conversion de Json a String*/
                StringWriter jsonstr=new StringWriter();
                JsonWriter writer = Json.createWriter(jsonstr);
                writer.writeObject(jsonObj);
                writer.close();
                jsonstr.close();
                return jsonstr.toString();
            }
        } catch (SQLException | IOException e) {
            throw new BDException (e.getMessage());
        }
        return null;
    }
    /**
     * Se obtiene la descripcion de un componente de la configuracionPC a partir de 
     * @param idConfiguracion
     * @return las descripciones de componentes
     * @throws BDException 
     */
    public static ArrayList<JsonObject> getDescComponentesEnConfiguracion (int idConfiguracion) throws BDException {
        String query = "SELECT * FROM APP.DESCRIPCIONCOMPONENTE, APP.TIPOCOMPONENTE, APP.COMPONENTESENCONFIGURACION"
                + " WHERE APP.DESCRIPCIONCOMPONENTE.IDDESCRIPCION = APP.COMPONENTESENCONFIGURACION.IDDESCRIPCION"
                + " AND APP.DESCRIPCIONCOMPONENTE.TIPO = APP.TIPOCOMPONENTE.IDTIPOCOMPONENTE AND APP.COMPONENTESENCONFIGURACION.IDCONFIGURACION = "+idConfiguracion;
        ResultSet rs = ConexionBD.getInstancia().ejecutaQuery(query);
        ArrayList<JsonObject> descripcionesComponentes = new ArrayList<> ();
        try {
            if(rs.next()){
                JsonObject jsonObj=Json.createObjectBuilder()
                .add("idDescripcion",rs.getString("IDDESCRIPCION"))
                .add("tipo",rs.getString("TIPO"))
                .add("marca",rs.getString("MARCA"))
                .add("modelo",rs.getString("MODELO"))
                .add("caracteristicasTecnicas",rs.getString("CARACTERISTICASTECNICAS"))
                .add("nombreTipoComponente",rs.getString("NOMBRETIPOCOMPONENTE"))
                .build();
                descripcionesComponentes.add(jsonObj);
            }
        } catch (SQLException e) {
            throw new BDException (e.getMessage());
        }
        return descripcionesComponentes;
    }
}
