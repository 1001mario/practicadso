/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import excepciones.BDException;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class GestorPersistenciaEmpleado {
    /**
     * Se obtiene un empleado mediante el nif
     * @param nif
     * @return el empleado a recuperar
     * @throws BDException 
     */
    public static String recuperaEmpleadoPorLogin(String nif) throws BDException {
        ResultSet rs;
        String sql = "SELECT * FROM APP.EMPLEADO WHERE NIFCIF ='"+nif+"'";
        
        try{
            /*Lectura de la BD y creación de la cadena Json*/
            rs = ConexionBD.getInstancia().ejecutaQuery(sql);
            
            if(rs.next()){
                JsonObject jsonObj=Json.createObjectBuilder()
                .add("nifCif",rs.getString("NIFCIF"))
                .add("rolEmpleado",rs.getString("ROL"))
                .add("fechaContratacion",rs.getString("FECHACONTRATACION"))
                .build();
                
                /*Conversion de Json a String*/
                StringWriter jsonstr=new StringWriter();
                JsonWriter writer = Json.createWriter(jsonstr);
                writer.writeObject(jsonObj);
                writer.close();
                jsonstr.close();
                return jsonstr.toString();
            }
        } catch(SQLException | IOException e) {
            throw new BDException(e.getMessage());
        }
        
        return null;
    }
}
