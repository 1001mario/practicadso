/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import excepciones.BDException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Representa la conexion de la base de datos.
 * 
 * Sigue un patron singleton (unica instancia) con el cual podemos
 * obtener la conexion por parte de diferentes partes de la aplicacion
 * en cualquier momento para realizar consultas o updates en la base de datos.
 * 
 * Para que la conexion funcione, es necesario crear por parte del usuario que
 * ejecute la aplicacion, una base de datos con jdbc llamada "ModelPC", con usuario
 * y password: root, schema: APP.
 * 
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ConexionBD {
    private final String urlBD = "jdbc:derby://localhost:1527/ModelPC";
    private final String userName = "root";
    private final String password = "root";
    private final String driverName = "org.apache.derby.jdbc.ClientDriver";
    private static Connection connection;
    private static ConexionBD conexion;
    
    /**
     * Constructor para la conexion con la base de datos
     * @throws BDException 
     */
    private ConexionBD () throws BDException {
        try{
            Class.forName(driverName);
        }catch(ClassNotFoundException e){
            throw new BDException("Error del Driver de la base de datos: " +e.getMessage());
        }
        try{
            /*creamos conexion*/
            connection = DriverManager.getConnection(urlBD, userName, password );
                 
        }catch(SQLException e){
            throw new BDException(e.getMessage());
        }
    }

    /**
     * Si no hay conexion la crea
     * @return la conexion
     * @throws BDException
     */
    public static ConexionBD getInstancia() throws BDException {
        if (conexion==null)
            
            conexion = new ConexionBD();
                   
        return conexion;
    }
    
    /**
     * Se ejecuta una consulta realizada en la BD.
     * @param sql
     * @return el resultado de la consulta a la BD
     * @throws BDException
     */
    public ResultSet ejecutaQuery(String sql) throws BDException{
        ResultSet resConsulta;
        /*creamos statement (el que ejecuta las querys)*/
        try{
            Statement statement = connection.createStatement();

            /*ejecutamos consultas*/
            resConsulta = statement.executeQuery(sql);
        }catch(SQLException e){
            // Lanzamos una excepcion de la base de datos.
            throw new BDException(e.getMessage());
        }
        return resConsulta;
    }
    
    /**
     * Inserta una tupla en una tabla de la BD.
     * @param sql
     * @return las tuplas modificadas
     * @throws BDException
     */
    public int ejecutaUpdate(String sql) throws BDException{
        /*creamos statement (el que ejecuta las querys)*/
        int lineasMod=0;
        try{
            Statement statement = connection.createStatement();
            lineasMod = statement.executeUpdate(sql); //ejecutamos consulta
            
        }catch(SQLException e){
            // Lanzamos una excepcion de la base de datos.
            throw new BDException(e.getMessage());
        }
        
        return lineasMod;
    }
}
