/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import excepciones.BDException;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class GestorPersistenciaUsuario {
    /**
     * Se obtine un usuario a partir del nifcif
     * @param nifcif
     * @param password
     * @return el usuario con el nifcif introducido
     * @throws BDException 
     */
    public static String getUsuarioByNifCif(String nifcif,String password) 
            throws BDException {
        ResultSet rs;
        String sql = "SELECT * FROM APP.USUARIO WHERE NIFCIF ='"+nifcif+"'AND PASSWORD='"+password+"'";
        try{
            /*Lectura de la BD y creación de la cadena Json*/
            rs = ConexionBD.getInstancia().ejecutaQuery(sql);
            //CAMBIAR 
            if(rs.next()){
                JsonObject jsonObj=Json.createObjectBuilder()
                .add("nifCif",rs.getString("NIFCIF"))
                .add("password",rs.getString("PASSWORD"))
                .add("nombre", rs.getString("NOMBRE"))
                .add("direccionPostal", rs.getString("DIRECCIONPOSTAL"))
                .add("direccionElectronica", rs.getString("DIRECCIONELECTRONICA"))
                .add("telefono", rs.getString("TELEFONO"))
                .build();

                /*Conversion de Json a String*/
                StringWriter jsonstr=new StringWriter();
                JsonWriter writer = Json.createWriter(jsonstr);
                writer.writeObject(jsonObj);
                writer.close();
                jsonstr.close();
                return jsonstr.toString();
            }
        } catch(SQLException | IOException e) {
            throw new BDException(e.getMessage());
        }
        
        return null;
    }
}
