/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import excepciones.BDException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.json.Json;
import javax.json.JsonObject;
/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class GestorPersistenciaComponente {
    /**
     * Se obtiene un componente a partir de su id
     * @param idComponente
     * @return si el componente es valido o no
     * @throws BDException 
     */
    public static boolean idValido (int idComponente) throws BDException {
        ResultSet rs;
        String sql = "SELECT * FROM APP.COMPONENTE WHERE ETIQUETAID ="+idComponente;
        
        try{
            /*Lectura de la BD y creación de la cadena Json*/
            rs = ConexionBD.getInstancia().ejecutaQuery(sql);
            return rs.next();
        }catch(SQLException e){
            throw new BDException(e.getMessage());
        }
    }
    /**
     * Se obtiene los componentes en stock a partir de su idDescripcion
     * @param idDescripcion
     * @return los componentes en stock
     * @throws BDException 
     */
    public static int getStockComponente(int idDescripcion) throws BDException {
        ResultSet rs;
        String sql = "SELECT COUNT(*) AS CONT FROM APP.COMPONENTE WHERE APP.COMPONENTE.IDDESCRIPCION ="+idDescripcion; 
        try{
            /*Lectura de la BD y creación de la cadena Json*/
            rs = ConexionBD.getInstancia().ejecutaQuery(sql);
            if (rs.next()) {
                return rs.getInt("CONT");
            }
        }catch(SQLException e){
            throw new BDException(e.getMessage());
        }
        return 0;
    }
    /**
     * Se obtiene los componentes disponibles a partir de idDescripcion
     * @param idDescripcion
     * @return componentes disponibles
     * @throws BDException 
     */
    public static JsonObject getComponenteDisponible(int idDescripcion) throws BDException {
        ResultSet rs;
        String sql = "SELECT * FROM APP.COMPONENTE WHERE APP.COMPONENTE.IDDESCRIPCION ="+idDescripcion; 
         try{
            /*Lectura de la BD y creación de la cadena Json*/
            rs = ConexionBD.getInstancia().ejecutaQuery(sql);
            if (rs.next()) {
                // Devolver el componente
                JsonObject jsonComponente = Json.createObjectBuilder()
                .add("etiquetaID",rs.getString("ETIQUETAID"))
                .add("fechaEntrada",rs.getString("FECHAENTRADA"))
                .add("reservado",rs.getString("RESERVADO"))
                .add("idDescripcion",rs.getString("IDDESCRIPCION"))
                .add("etiquetaPC",rs.getInt("ETIQUETAPC"))
                .add("recibidoEnCompra",rs.getString("RECIBIDOENCOMPRA"))
                .add("ubicacion",rs.getInt("UBICACION"))
                .add("tipo",rs.getString("TIPO"))
                .add("marca",rs.getString("MARCA"))
                .add("modelo",rs.getString("MODELO"))
                .add("precio",rs.getInt("PRECIO"))
                .add("caracteristicasTecnicas",rs.getString("CARACTERISTICASTECNICAS"))
                .add("seccion",rs.getInt("SECCION"))
                .add("zona",rs.getInt("ZONA"))
                .add("estanteria",rs.getInt("ESTANTERIA"))
                .add("ocupado",rs.getInt("OCUPADO"))
                .build();
                return jsonComponente;
            }
        }catch(SQLException e){
            throw new BDException(e.getMessage());
        }
        return null;
    }
    /**
     * Se obtiene los componentes reservados a partir de idComponente
     * @param idComponente
     * @return los componentes reservados
     * @throws BDException 
     */
    public static JsonObject getComponenteReservado (int idComponente) throws BDException {
        ResultSet rs;
        String sql = "SELECT * FROM APP.COMPONENTE, APP.DESCRIPCIONCOMPONENTE, APP.PC, APP.ESPACIOALMACENAMIENTO "
                + "WHERE APP.COMPONENTE.ETIQUETAID ="+idComponente+" AND APP.COMPONENTE.IDDESCRIPCION = APP.DESCRIPCIONCOMPONENTE.IDDESCRIPCION"
                + " AND APP.COMPONENTE.ETIQUETAPC = APP.PC.ETIQUETAID AND APP.COMPONENTE.UBICACION = APP.ESPACIOALMACENAMIENTO.IDUBICACION AND ETIQUETAPC = NULL";
        
        try{
            /*Lectura de la BD y creación de la cadena Json*/
            rs = ConexionBD.getInstancia().ejecutaQuery(sql);
            if (rs.next()) {
                // Devolver el componente
                JsonObject jsonComponente = Json.createObjectBuilder()
                .add("etiquetaID",rs.getString("ETIQUETAID"))
                .add("fechaEntrada",rs.getString("FECHAENTRADA"))
                .add("reservado",rs.getString("RESERVADO"))
                .add("idDescripcion",rs.getString("IDDESCRIPCION"))
                .add("etiquetaPC",rs.getInt("ETIQUETAPC"))
                .add("recibidoEnCompra",rs.getString("RECIBIDOENCOMPRA"))
                .add("ubicacion",rs.getInt("UBICACION"))
                .add("tipo",rs.getString("TIPO"))
                .add("marca",rs.getString("MARCA"))
                .add("modelo",rs.getString("MODELO"))
                .add("precio",rs.getInt("PRECIO"))
                .add("caracteristicasTecnicas",rs.getString("CARACTERISTICASTECNICAS"))
                .add("seccion",rs.getInt("SECCION"))
                .add("zona",rs.getInt("ZONA"))
                .add("estanteria",rs.getInt("ESTANTERIA"))
                .add("ocupado",rs.getInt("OCUPADO"))
                .build();
                
                return jsonComponente;
            }
        }catch(SQLException e){
            throw new BDException(e.getMessage());
        }
        return null;
    }
    /**
     * Se obtiene la idDescripcion de un componente a partir de idComponente
     * @param idComponente
     * @return la idDescripcion de un componente
     * @throws BDException 
     */
    public static int getIdDescripcion (int idComponente) throws BDException {
        ResultSet rs;
        String sql = "SELECT IDDESCRIPCION FROM APP.COMPONENTE WHERE ETIQUETAID ="+idComponente;
        
        try{
            /*Lectura de la BD y creación de la cadena Json*/
            rs = ConexionBD.getInstancia().ejecutaQuery(sql);
            while (rs.next()) {
                return rs.getInt("IDDESCRIPCION");
            }
        }catch(SQLException e){
            throw new BDException(e.getMessage());
        }
        return 0;
    }
    /**
     * Se actualiza un PC a partir de 
     * @param etiquetaComponente
     * @param etiquetaPC
     * @return el PC actualizado
     * @throws BDException 
     */
    public static int actualizarPC (int etiquetaComponente, int etiquetaPC) throws BDException {
        String sql = "UPDATE APP.COMPONENTE SET ETIQUETAPC = "+etiquetaPC+" WHERE ETIQUETAID = "+etiquetaComponente;
        int rs;
        try{
            rs = ConexionBD.getInstancia().ejecutaUpdate(sql);
        }catch(SQLException e){
            throw new BDException(e.getMessage());
        }
        return rs;
    }
    /**
     * Se actuliza un componente a partir de
     * @param etiquetaComponente
     * @throws BDException 
     */
    public static void reservar(int etiquetaComponente) throws BDException {
        String sql = "UPDATE APP.COMPONENTE SET RESERVADO = 1 WHERE ETIQUETAID = "+etiquetaComponente;
        try{
            ConexionBD.getInstancia().ejecutaUpdate(sql);
        }catch(SQLException e){
            throw new BDException(e.getMessage());
        }
    }
}
