/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import excepciones.BDException;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class GestorPersistenciaEspacioAlmacenamiento {
    
    /**
     * Se obtiene el espacio de almacenamiento a partir de
     * @param idUbicacion
     * @return el espacio de almacenamiento de idUbicacion
     * @throws BDException 
     */
    public static String getEspacioAlmacen (int idUbicacion) throws BDException {
        String query = "SELECT * FROM APP.ESPACIOALMACENAMIENTO WHERE IDUBICACION = "+idUbicacion+
                "AND APP.ESPACIOALMACENAMIENTO.OCUPADO = 0";
        ResultSet rs = ConexionBD.getInstancia().ejecutaQuery(query);
        try {
            if(rs.next()){
                JsonObject jsonObj=Json.createObjectBuilder()
                .add("idUbicacion",rs.getInt("IDUBICACION"))
                .add("seccion",rs.getInt("SECCION"))
                .add("zona",rs.getInt("ZONA"))
                .add("estanteria",rs.getInt("ESTANTERIA"))
                .add("ocupado",rs.getInt("OCUPADO"))
                .build();
                
                /*Conversion de Json a String*/
                StringWriter jsonstr=new StringWriter();
                JsonWriter writer = Json.createWriter(jsonstr);
                writer.writeObject(jsonObj);
                writer.close();
                jsonstr.close();
                return jsonstr.toString();
            }
        } catch (SQLException | IOException e) {
            throw new BDException (e.getMessage());
        }
        return null;
    }
    /**
     * Se obtiene los espacios de almacenamiento que no estan ocupados
     * @return los espacios de almacenamiento no ocupados
     * @throws BDException 
     */
    public static JsonObject[] getEspacio () throws BDException {
        String query = "SELECT * FROM APP.ESPACIOALMACENAMIENTO WHERE OCUPADO = 0";
        ResultSet rs = ConexionBD.getInstancia().ejecutaQuery(query);
        ArrayList<JsonObject> jsonEspAlmacenamiento = new ArrayList<> ();
        try {
            while(rs.next()){
                JsonObject jsonObj=Json.createObjectBuilder()
                .add("idUbicacion",rs.getInt("IDUBICACION"))
                .add("seccion",rs.getInt("SECCION"))
                .add("zona",rs.getInt("ZONA"))
                .add("estanteria",rs.getInt("ESTANTERIA"))
                .add("ocupado",rs.getInt("OCUPADO"))
                .build();
                jsonEspAlmacenamiento.add(jsonObj);
            }
        } catch (SQLException e) {
            throw new BDException (e.getMessage());
        }
        JsonObject[] espacios = new JsonObject[jsonEspAlmacenamiento.size()];
        jsonEspAlmacenamiento.toArray(espacios);
        return espacios;
    }
}
