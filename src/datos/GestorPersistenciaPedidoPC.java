/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import excepciones.BDException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class GestorPersistenciaPedidoPC {
    
    /**
     * Se obtienen los pedidos a partir de
     * @param estado
     * @return los pedidos segun el estado
     * @throws BDException 
     */    
    public static JsonObject[] getPedidos(int estado) throws BDException {
        String query = "SELECT * FROM APP.PEDIDOPC, APP.CONFIGURACIONPC, APP.CPU "
                + "WHERE APP.PEDIDOPC.ESTADO = "+estado+" AND APP.PEDIDOPC.CONFIGURACIONSOLICITADA = APP.CONFIGURACIONPC.IDCONFIGURACION "
                + "AND APP.CONFIGURACIONPC.TIPOCPU = APP.CPU.IDTIPOCPU";

        
        ResultSet rs = ConexionBD.getInstancia().ejecutaQuery(query);

        ArrayList<JsonObject> jsonPedidos = new ArrayList<> ();
        
        try {
            while (rs.next()) {
                JsonObject jsonObj = (Json.createObjectBuilder()
                    .add("idPedido",rs.getString("IDPEDIDO"))
                    .add("cantidadSolicitada",rs.getString("CANTIDADSOLICITADA"))
                    .add("estado",rs.getString("ESTADO"))
                    .add("encargadoPor",rs.getString("ENCARGADOPOR"))
                    .add("idConfiguracion",rs.getString("IDCONFIGURACION"))
                    .add("velocidadCPU",rs.getString("VELOCIDADCPU"))
                    .add("capacidadRAM",rs.getString("CAPACIDADRAM"))
                    .add("capacidadDD",rs.getString("CAPACIDADDD"))
                    .add("velocidadTarjetaGrafica",rs.getString("VELOCIDADTARJETAGRAFICA"))
                    .add("memoriaTarjetaGrafica",rs.getString("MEMORIATARJETAGRAFICA"))
                    .add("idtipoCPU",rs.getString("IDTIPOCPU"))
                    .add("nombreTipoCPU",rs.getString("NOMBRETIPOCPU"))
                    .build());
                jsonPedidos.add(jsonObj);
            }
        } catch(SQLException e) {
            throw new BDException(e.getMessage());
        }
        JsonObject[] pedidos = new JsonObject[jsonPedidos.size()];
        jsonPedidos.toArray(pedidos);
        return pedidos;
    }
}
