/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public enum CPU {
    AMD, IntelCore;
    
    /**
     * Trasnforma una cadena correspondiente en el valor de getTipo
     * que le corresponda.
     * @param tipoCPU cadena de caracteres que indica el estado
     * @return devuelve una valor del tipo estado
     */
    public static CPU getTipo (String tipoCPU) {
        switch (tipoCPU) {
            case "1": return AMD;
            case "2": return IntelCore;
            default: return null;
        }
    }
}
