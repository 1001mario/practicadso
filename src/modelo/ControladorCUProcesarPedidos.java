/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import excepciones.BDException;
import excepciones.PedidosNotFoundException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ControladorCUProcesarPedidos {
    private static ControladorCUProcesarPedidos controladorCU;
    
    /**
     *Obtiene la instancia especifica del controlador del CU procesar pedidos.
     * @return controladorCU
     */
    public static ControladorCUProcesarPedidos getControlador () {
        if (controladorCU == null) {
            controladorCU = new ControladorCUProcesarPedidos ();
        }
        return controladorCU;
    }
    
    /**
     * Se obtiene la lista de pedidos solicitados
     * @return lista pedidosCU
     * @throws PedidosNotFoundException
     */
    public PedidoPC[] getListaPedidos () throws PedidosNotFoundException {
        PedidoPC[] pedidosCU;
        try {
            pedidosCU = PedidoPC.getPedidos(EstadoVentaPCs.Solicitado);
        } catch (BDException e) {
            throw new PedidosNotFoundException (e.getMessage());
        }
        return pedidosCU;
    }

    /**
     * Se reserva un PC en stock
     * @param pedido
     * @return el numero de PCs en stock
     */
    public int reservarPCsStock(PedidoPC pedido) {
        PC pcsStock[] = pedido.getConfiguracion().getPCsEnStock();
        int cantidad = pedido.getCantidadSolicitada();
        if(pcsStock.length>=cantidad) {
            for(int i=0;i<cantidad;i++) {
                pcsStock[i].reservar();
            }
            pedido.setEstado(EstadoVentaPCs.Completado);
        }
        else {
            for (PC pc : pcsStock) {
                pc.reservar();
            }
            pedido.setEstado(EstadoVentaPCs.EnProceso);
        }
        return pcsStock.length;
    }

    /**
     * Se comprueba es stock de componentes
     * @param componentesEnConfiguracion
     * @return si esta disponible es componente en stock
     */
    public boolean checkStockComponentes(ArrayList<DescripcionComponente> componentesEnConfiguracion) {
        boolean flag = true;
        for(int i=0; i<componentesEnConfiguracion.size() && flag;i++) {
            DescripcionComponente comp = componentesEnConfiguracion.get(i);
            if (comp.getStock()==0) flag=false;
        }
        return flag;
    }
    
    /**
     * Se reserva los componentes para un pedidoPC
     * @param pedido
     * @return si se ha reservado el componente para el pedidoPC
     */
    public boolean reservarComponentes(PedidoPC pedido) {
        ArrayList<DescripcionComponente> componentesEnConfiguracion;
        try {
            componentesEnConfiguracion = pedido.getConfiguracion().getDescComponentes();
            
            boolean hayStock = checkStockComponentes(componentesEnConfiguracion);
            if(hayStock) {
                for(DescripcionComponente descComp: componentesEnConfiguracion) {
                    Componente cmp = descComp.getComponenteEnStock();
                    cmp.reservar();
                }
                return true;
            }
            
        } catch (BDException ex) {
            Logger.getLogger(ControladorCUProcesarPedidos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;  
    }

    
}
