/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import excepciones.BDException;
import excepciones.EmpNotFoundException;

/**
 *
 * Esto es un singleton, todos los controladores de casos de uso necesitan
 * seguir el patron singleton de la arquitectura software.
 * 
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ControladorCUIdentificarse {
    private static ControladorCUIdentificarse controladorCU;
    
    /**
     * Obtiene la instancia especifica del controlador del CU identificarse.
     * 
     * @return ControladorCUIdentificarse
     */
    public static ControladorCUIdentificarse getControlador() {
        if (controladorCU == null) {
            controladorCU = new ControladorCUIdentificarse();
        }
        return controladorCU;
    }
    
   /**
     * Se comprueba si se realiza Identificarse correctamente.
     * 
     * @param nif
     * @param password
     * @return un tipo de empleado
     * @throws EmpNotFoundException
     */
    public RolEnLaEmpresa identificarse(String nif, String password) 
            throws EmpNotFoundException {
        
        String usuario;
        Empleado empleado;
        
        RolEnLaEmpresa tipo=null;
        
        try{    
            usuario = Usuario.obtenerUsuario(nif, password);
            if(usuario == null){
                throw new EmpNotFoundException("Login o password Incorrectos.");
            }
            empleado = Empleado.obtenerEmpleado(usuario);
            if(empleado == null){
                throw new EmpNotFoundException("El usuario no es un empleado correcto.");
            }
            tipo = empleado.getRolEmpleado();
            
        }catch(BDException ex){
            throw new EmpNotFoundException("Empleado no Encontrado: "+ex.getMessage());   
        }
        
        return tipo;
    }
}
