/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import datos.GestorPersistenciaPedidoPC;
import excepciones.BDException;
import javax.json.JsonObject;


/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class PedidoPC {
    private int idPedido;
    private int cantidadSolicitada;
    private EstadoVentaPCs estado;
    private ConfiguracionPC configuracion;
    private String nifCifEmpresa;
    
    /**
     * Constructor no vacio de PedidoPC
     * @param idPedido
     * @param cantidadSolicitada
     * @param estado
     * @param configuracion
     * @param nifCifEmpresa
     */
    public PedidoPC (int idPedido, int cantidadSolicitada, EstadoVentaPCs estado,
            ConfiguracionPC configuracion, String nifCifEmpresa) {
        this.idPedido = idPedido;
        this.cantidadSolicitada = cantidadSolicitada;
        this.estado = estado;
        this.configuracion = configuracion;
        this.nifCifEmpresa = nifCifEmpresa;
    }
    
    /**
     * Se obtiene los pedidos dependiendo el estado
     * @param estado
     * @return lista de pedidoPC
     * @throws BDException
     */
    public static PedidoPC[] getPedidos(EstadoVentaPCs estado) throws BDException {
        String nifCifEmpresa;
        CPU tipoCPU;
        int idPedido, cantidadSolicitada, idConfiguracion, capacidadRAM, capacidadDD;
        double velocidadCPU, velocidadTarjetaGrafica, memoriaTarjetaGrafica;
        ConfiguracionPC configuracion;
        JsonObject[] jsonPedidos = null;
        
        switch (estado) {
            case EnProceso: jsonPedidos = GestorPersistenciaPedidoPC.getPedidos(2); break;
            case Solicitado: jsonPedidos = GestorPersistenciaPedidoPC.getPedidos(1); break;
        }
        
        PedidoPC[] pedidos = new PedidoPC[jsonPedidos.length];
        for (int i = 0; i < jsonPedidos.length; i++) {
            idPedido = Integer.parseInt(jsonPedidos[i].getString("idPedido"));
            cantidadSolicitada = Integer.parseInt(jsonPedidos[i].getString("cantidadSolicitada"));
            nifCifEmpresa = jsonPedidos[i].getString("encargadoPor");
            idConfiguracion = Integer.parseInt(jsonPedidos[i].getString("idConfiguracion"));
            tipoCPU = CPU.getTipo(jsonPedidos[i].getString("idtipoCPU"));
            velocidadCPU = Double.parseDouble(jsonPedidos[i].getString("velocidadCPU"));
            capacidadRAM = Integer.parseInt(jsonPedidos[i].getString("capacidadRAM"));
            capacidadDD = Integer.parseInt(jsonPedidos[i].getString("capacidadDD"));
            velocidadTarjetaGrafica = Double.parseDouble(jsonPedidos[i].getString("velocidadTarjetaGrafica"));
            memoriaTarjetaGrafica = Double.parseDouble(jsonPedidos[i].getString("memoriaTarjetaGrafica"));
            configuracion = new ConfiguracionPC(idConfiguracion, tipoCPU, velocidadCPU, 
                    capacidadRAM, capacidadDD, velocidadTarjetaGrafica, memoriaTarjetaGrafica);
            pedidos[i] = new PedidoPC(idPedido, cantidadSolicitada, estado, configuracion, nifCifEmpresa);
        }
        return pedidos;
    }
    
    /**
     * Se obtiene la configuracion de un PC
     * @return configuracion
     */
    public ConfiguracionPC getConfiguracion () {
        return configuracion;
    }

    /**
     * Se obtiene el identificador de un PedidoPC
     * @return idPedido
     */
    public int getIdPedido() {
        return idPedido;
    }

    /**
     * Se obtiene el estado de un PedidoPC
     * @return estado
     */
    public Object getEstado() {
        return estado;
    }
    
    /**
     * Se obtiene la cantidad solicitada de un PedidoPC
     * @return cantidadSolicitada
     */
    public int getCantidadSolicitada() {
        return cantidadSolicitada;
    }

    /**
     * Permite fijar el estado del pedido
     * @param estadoVentaPCs 
     */
    void setEstado(EstadoVentaPCs estadoVentaPCs) {
        this.estado = estadoVentaPCs;
    }
    
}
