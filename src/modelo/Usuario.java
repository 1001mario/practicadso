/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import excepciones.BDException;
import datos.GestorPersistenciaUsuario;
import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class Usuario {

    private String nifCif;
    private String nombre;
    private String direccion;
    private String email;
    private String telefono;
    private String password;

    /**
     * Constructor no vacio de Usuario
     * @param jsonUsuario
     */
    public Usuario(String jsonUsuario) {
         /*Conversion de String a Json*/
        StringReader strReader=new StringReader(jsonUsuario);
        JsonReader jReader=Json.createReader(strReader);
        JsonObject jsonObject=jReader.readObject();
        
        setNombre(jsonObject.getString("nombre"));
        setNifCif(jsonObject.getString("nifCif"));
        setEmail(jsonObject.getString("direccionElectronica"));
        setTelefono(jsonObject.getString("telefono"));
        setDireccion(jsonObject.getString("direccionPostal"));
        setPassword(jsonObject.getString("password"));

    }
   
    /**
     * Se obtiene el nombre de usuario
     * @return nombre
     */
    public String getNombre () {
        return nombre;
    }
    
    /**
     * Se obtiene el password
     * @return password
     */
    public String getPassword () {
        return password;
    }

    /**
     * Se obtiene el nifCif
     * @return nifcif
     */
    public String getNifCif(){
        return nifCif;
    }

    /**
     *Se obtiene la direccion postal
     * @return direccion
     */
    public String getDireccion(){
        return direccion;
    }
    
    /**
     * Se obtiene el correo electronico
     * @return email
     */
    public String getEmail(){
        return email;
    }
    
    /**
     * se obtiene el telefono del usuario
     * @return telefono
     */
    public String getTelefono(){
        return telefono;
    }
    /**
     * Establece el nombre del usuario
     * @param nombre 
     */
    private void setNombre(String nombre){
        this.nombre=nombre;
    }
    /**
     * Se establece el password del usuario
     * @param password 
     */
    private void setPassword(String password){
        this.password=password;
    }
    /**
     * Se establece el nifcif del usuario
     * @param nifCif 
     */
    private void setNifCif(String nifCif){
        this.nifCif=nifCif;
    }
    /**
     * Se establece la direccion postal del usuario
     * @param direccion 
     */
    private void setDireccion(String direccion){
        this.direccion = direccion;
    }
    /**
     * Se establece el correo electronico del usuario
     * @param email 
     */
    private void setEmail(String email){
        this.email=email;
    }
    /**
     * Se establece el telefono del usuario
     * @param telefono 
     */
    private void setTelefono(String telefono){
        this.telefono=telefono;
    }
    
    /**
     * Se obtiene el Usuario a partir de
     * @param nifCif
     * @param password
     * @return un usuario
     * @throws BDException
     */
    protected static String obtenerUsuario (String nifCif,String password) 
            throws BDException {
        String jsonUsuario = GestorPersistenciaUsuario.getUsuarioByNifCif(nifCif,password);
              
        return jsonUsuario;
    }
}
