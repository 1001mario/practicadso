/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public enum EstadoVentaPCs {
    Solicitado, EnProceso, Completado, Enviado, Entregado;

    public static EstadoVentaPCs getEstado(String estado) {
        switch (estado) {
            case "1": return Solicitado;
            case "2": return EnProceso;
            case "3": return Completado;
            case "4": return Enviado;
            case "5": return Entregado;
            default: return null;
        }
    }
}
