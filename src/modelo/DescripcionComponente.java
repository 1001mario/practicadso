/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import datos.GestorPersistenciaComponente;
import excepciones.BDException;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonObject;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class DescripcionComponente {
    private int idDescripcion;
    private String marca;
    private String modelo;
    private float precio;
    private String caracteristicasTecnicas;
    private TipoComponente tipo;
    
    /**
     * Constructor no vacio de DescripcionComponente
     * @param idDescripcion
     * @param marca
     * @param modelo
     * @param precio
     * @param caracteristicasTecnicas
     * @param tipo
     */
    public DescripcionComponente (int idDescripcion, String marca, String modelo, float precio,
            String caracteristicasTecnicas, TipoComponente tipo) {
        this.idDescripcion = idDescripcion;
        this.marca = marca;
        this.modelo = modelo;
        this.precio = precio;
        this.caracteristicasTecnicas = caracteristicasTecnicas;
        this.tipo = tipo;
    }
    
    /**
     * Se obtiene idDescripcion
     * @return idDescripcion
     */
    public int getIdDescripcion () {
        return idDescripcion;
    }
    
    /**
     * Se obtiene los componentes que estan en stock
     * @return stock
     */
    public int getStock() {
        int stock = 0;
        try {
            stock = GestorPersistenciaComponente.getStockComponente(idDescripcion);
        } catch (BDException ex) {
            Logger.getLogger(DescripcionComponente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stock;
    }
    
    @Override
    public boolean equals(Object o) {
        DescripcionComponente descripcion = (DescripcionComponente) o;
        return idDescripcion == descripcion.idDescripcion;
    }

    /**
     * Se obtiene los componentes en stock a oartir de Json
     * @return un componente
     */
    public Componente getComponenteEnStock() {
        JsonObject jsonComponente;
        Componente cmp = null;
        try {
            jsonComponente = GestorPersistenciaComponente.getComponenteDisponible(idDescripcion);
            int etiquetaId = Integer.parseInt(jsonComponente.getString("etiquetaId"));
            boolean reservado = Integer.parseInt(jsonComponente.getString("reservado"))==1;
            int idUbicacion = jsonComponente.getInt("ubicacion");
            int seccion = jsonComponente.getInt("seccion");
            int zona = jsonComponente.getInt("zona");
            int estanteria = jsonComponente.getInt("estanteria");
            int ocupado = jsonComponente.getInt("ocupado");
            Date fechaEntrada = Date.valueOf(jsonComponente.getString("fechaEntrada"));
            EspacioAlmacenamiento espacio = new EspacioAlmacenamiento(idUbicacion, seccion, zona, estanteria, ocupado);
            cmp = new Componente(etiquetaId, reservado, fechaEntrada, this, espacio);
        } catch (BDException ex) {
            Logger.getLogger(DescripcionComponente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cmp;
    }
}
