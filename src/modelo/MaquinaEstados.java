/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.JFrame;
import vista.VistaLogin;
import vista.VistaOpcionesEncargadoAlmacen;
import vista.VistaOpcionesGerenteVentas;
import vista.VistaOpcionesTecnicoTaller;

/**
 * Se aplica el patron singleton para obtener solamente una maquina de
 * estados en tiempo de ejecucion y el patron state para el cambio de jframes
 * y ventanas.
 * 
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class MaquinaEstados {
    private static MaquinaEstados maquina;
    private JFrame jFrameMenu;
    private JFrame jFrameAnterior;
    private JFrame jFrame;
    
    private MaquinaEstados () {
        jFrame = new VistaLogin();
    }
    
    public static MaquinaEstados getMaquinaEstados () {
        if (maquina == null) {
            maquina = new MaquinaEstados();
        }
        return maquina;
    }
    
    public void setEstado (JFrame jFrame) {
        if (jFrame instanceof VistaOpcionesEncargadoAlmacen || jFrame instanceof VistaOpcionesTecnicoTaller 
                || jFrame instanceof VistaOpcionesGerenteVentas) {
            jFrameMenu = jFrame;
        }
        jFrameAnterior = this.jFrame;
        this.jFrame = jFrame;
    }
    
    public void actualiza () {
        if (jFrameAnterior != null) {
            jFrameAnterior.dispose();
        }
        jFrame.setVisible(true);
    }
    
    public void volverMenu () {
        jFrame.dispose();
        jFrameMenu.setVisible(true);
        jFrame = jFrameMenu;
    }
}
