/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import datos.GestorPersistenciaEspacioAlmacenamiento;
import excepciones.BDException;
import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class EspacioAlmacenamiento {
    private int idUbicacion;
    private int seccion;
    private int zona;
    private int estanteria, ocupado;

    /**
     * Se obtiene la identificacion de ubicacion
     * @return idUbicacion
     */
    public int getIdUbicacion() {
        return idUbicacion;
    }
    /**
     * Establece la identificacion de ubicacion
     * @param idUbicacion 
     */
    private void setIdUbicacion(int idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    /**
     * Se obtiene la seccion de un espacio de almacenamiento
     * @return seccion
     */
    public int getSeccion() {
        return seccion;
    }
    /**
     * Se establece la seccion de un espacio de almacenamiento
     * @param seccion 
     */
    private void setSeccion(int seccion) {
        this.seccion = seccion;
    }

    /**
     *Se obtiene la zona de un espacio de almacenamiento
     * @return zona
     */
    public int getZona() {
        return zona;
    }
    /**
     * Se establece la zona de un espacio de almacenamiento
     * @param zona 
     */
    private void setZona(int zona) {
        this.zona = zona;
    }

    /**
     *Se obtiene la estanteria de un espacio de almacenamiento
     * @return estanteria
     */
    public int getEstanteria() {
        return estanteria;
    }
    /**
     * Se establece la estanteria de un espacio de almacenamiento
     * @param estanteria 
     */
    private void setEstanteria(int estanteria) {
        this.estanteria = estanteria;
    }

    /**
     *Se obtiene si esta ocupado un espacio de almacenamiento
     * @return ocupado
     */
    public int getOcupado() {
        return ocupado;
    }
    /**
     * Se establece si esta ocupado un espacio de almacenamiento
     * @param ocupado 
     */
    private void setOcupado(int ocupado) {
        this.ocupado = ocupado;
    }

    /**
     * Contructor no vacio de Espacio de Almacenamiento de objetos
     * @param idUbicacion
     * @param seccion
     * @param zona
     * @param estanteria
     * @param ocupado
     */
    public EspacioAlmacenamiento(int idUbicacion, int seccion, int zona, int estanteria, int ocupado) {
        this.idUbicacion = idUbicacion;
        this.seccion = seccion;
        this.zona = zona;
        this.estanteria = estanteria;
        this.ocupado = ocupado;
    }
    
    /**
     *Contructor no vacio de Espacio de Almacenamiento de Json
     * @param jsonEspAlmacenamiento
     */
    public EspacioAlmacenamiento (String jsonEspAlmacenamiento) {
       JsonObject jsonObject = Json.createReader(new StringReader(jsonEspAlmacenamiento)).readObject();
        
        setEstanteria(jsonObject.getInt("estanteria"));
        setIdUbicacion(jsonObject.getInt("idUbicacion"));
        setSeccion( jsonObject.getInt("seccion"));
        setOcupado( jsonObject.getInt("ocupado"));
        setZona( jsonObject.getInt("zona"));
    }
    
    /**
     * Se obtiene el espacio de almacenamiento por el idUbicacion
     * @param idUbi
     * @return espalmacenamiento
     */
    public static EspacioAlmacenamiento obtenerEspAlmacenamientoById(int idUbi){
        
        EspacioAlmacenamiento espalmacenamiento = null;
        try {
            String jsonEspAlmacenamiento = GestorPersistenciaEspacioAlmacenamiento.getEspacioAlmacen(idUbi);
            StringReader strReader = new StringReader(jsonEspAlmacenamiento);
            JsonReader jReader=Json.createReader(strReader);
            JsonObject jsonObject = jReader.readObject();
            espalmacenamiento = new EspacioAlmacenamiento(jsonEspAlmacenamiento);
        } catch (BDException e) {
            e.printStackTrace();
        }
        return espalmacenamiento;

    }
    
    /**
     * Se obtienen los espacios de almacenamiento
     * @return espacio
     * @throws BDException
     */
    public static EspacioAlmacenamiento[] getEspacios() throws BDException {
        
        int idUbicacion;
        int seccion;
        int zona;
        int estanteria, ocupado;
        JsonObject[] jsonEspAlmacenamiento = null;
        
        jsonEspAlmacenamiento = GestorPersistenciaEspacioAlmacenamiento.getEspacio();
        
        EspacioAlmacenamiento[] espacio = new EspacioAlmacenamiento[jsonEspAlmacenamiento.length];
        for (int i = 0; i < jsonEspAlmacenamiento.length; i++) {
            idUbicacion = jsonEspAlmacenamiento[i].getInt("idUbicacion");
            seccion = jsonEspAlmacenamiento[i].getInt("seccion");
            zona = jsonEspAlmacenamiento[i].getInt("zona");
            estanteria = jsonEspAlmacenamiento[i].getInt("seccion");
            ocupado = jsonEspAlmacenamiento[i].getInt("ocupado");
            espacio[i] = new EspacioAlmacenamiento(idUbicacion,seccion, zona, estanteria, ocupado);
        }
        return espacio;
    }
}
