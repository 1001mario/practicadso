/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class Material {
    private String etiquetaId;
    private boolean reservado;
    private EspacioAlmacenamiento ubicacion;
    
    /**
     * Constructor no vacio de Material
     * @param etiquetaId
     * @param ubicacion
     */
    public Material (String etiquetaId, EspacioAlmacenamiento ubicacion) {
        this.etiquetaId = etiquetaId;
        this.ubicacion = ubicacion;
        reservado = false;
    }
}
