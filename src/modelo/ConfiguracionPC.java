/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import datos.GestorPersistenciaPC;
import datos.GestorPersistenciaConfiguracionPC;
import excepciones.BDException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ConfiguracionPC {
    private int idConfiguracion;
    private CPU tipoCPU;
    private double velocidadCPU;
    private int capacidadRAM;
    private int capacidadDD;
    private double velocidadTarjetaGrafica;
    private double memoriaTarjetaGrafica;
    
    /**
     * Constructor no vacio de ConfiguracionPC con objetos
     * @param idConfiguracion
     * @param tipoCPU
     * @param velocidadCPU
     * @param capacidadRAM
     * @param capacidadDD
     * @param velocidadTarjetaGrafica
     * @param memoriaTarjetaGrafica
     */
    public ConfiguracionPC (int idConfiguracion, CPU tipoCPU, double velocidadCPU, int capacidadRAM, 
            int capacidadDD, double velocidadTarjetaGrafica, double memoriaTarjetaGrafica) {
        this.idConfiguracion = idConfiguracion;
        this.tipoCPU = tipoCPU;
        this.velocidadCPU = velocidadCPU;
        this.capacidadRAM = capacidadRAM;
        this.capacidadDD = capacidadDD;
        this.velocidadTarjetaGrafica = velocidadTarjetaGrafica;
        this.memoriaTarjetaGrafica = memoriaTarjetaGrafica;
    }
    
    /**
     * Constructor no vacio de ConfiguracionPC mediante Json
     * @param jsonConfiguracion
     */
    public ConfiguracionPC (JsonObject jsonConfiguracion) {
        this.idConfiguracion = Integer.parseInt(jsonConfiguracion.getString("idConfiguracion"));
        this.tipoCPU = CPU.getTipo(jsonConfiguracion.getString("tipoCPU"));
        this.velocidadCPU = Double.valueOf(jsonConfiguracion.getString("velocidadCPU"));
        this.capacidadRAM = Integer.parseInt(jsonConfiguracion.getString("capacidadRAM"));
        this.capacidadDD = Integer.parseInt(jsonConfiguracion.getString("capacidadDD"));
        this.velocidadTarjetaGrafica = Double.valueOf(jsonConfiguracion.getString("velocidadTarjetaGrafica"));
        this.memoriaTarjetaGrafica = Double.valueOf(jsonConfiguracion.getString("memoriaTarjetaGrafica"));
    }
    
    /**
     * Se obtine la configuracion de Pc a partir de
     * @param idConfiguracion
     * @return la configuracionPC
     */
    public static ConfiguracionPC obtenerConfiguracionPCbyID (int idConfiguracion) {
        ConfiguracionPC configuracionPC = null;
        try {
            String stringConfiguracion = GestorPersistenciaConfiguracionPC.getConfiguracion(idConfiguracion);
            StringReader strReader = new StringReader(stringConfiguracion);
            JsonReader jReader=Json.createReader(strReader);
            JsonObject jsonConfiguracion = jReader.readObject();
            configuracionPC = new ConfiguracionPC(jsonConfiguracion);
        } catch (BDException e) {
            e.printStackTrace();
        }
        return configuracionPC;
    }

    /**
     * Se obtiene la idConfiguracion
     * @return el identifcador de la configuracion
     */
    public int getIdConfiguracion() {
        return idConfiguracion;
    }

    /**
     * Se obtiene el tipo CPU
     * @return tipoCPU
     */
    public CPU getTipoCPU() {
        return tipoCPU;
    }

    /**
     * Establece el tipo de CPU
     * @param tipoCPU
     */
    public void setTipoCPU(CPU tipoCPU) {
        this.tipoCPU = tipoCPU;
    }

    /**
     * Se obtiene la velocidad de la CPU
     * @return velocidadCPU
     */
    public double getVelocidadCPU() {
        return velocidadCPU;
    }

    /**
     * Establece la velocidad de la CPU
     * @param velocidadCPU
     */
    public void setVelocidadCPU(double velocidadCPU) {
        this.velocidadCPU = velocidadCPU;
    }

    /**
     * Se obtiene la capacidad RAM 
     * @return capacidadRAM
     */
    public int getCapacidadRAM() {
        return capacidadRAM;
    }

    /**
     * Establece la capacidad RAM
     * @param capacidadRAM
     */
    public void setCapacidadRAM(int capacidadRAM) {
        this.capacidadRAM = capacidadRAM;
    }

    /**
     * Se obtiene la capacidad DD
     * @return capacidadDD
     */
    public int getCapacidadDD() {
        return capacidadDD;
    }

    /**
     * Establece la capacidad DD
     * @param capacidadDD
     */
    public void setCapacidadDD(int capacidadDD) {
        this.capacidadDD = capacidadDD;
    }

    /**
     * Se obtiene la velocidad de la tarjeta grafica
     * @return velocidadTarjetaGrafica
     */
    public double getVelocidadTarjetaGrafica() {
        return velocidadTarjetaGrafica;
    }

    /**
     * Establece la velocidad de la tarjeta grafica
     * @param velocidadTarjetaGrafica
     */
    public void setVelocidadTarjetaGrafica(double velocidadTarjetaGrafica) {
        this.velocidadTarjetaGrafica = velocidadTarjetaGrafica;
    }

    /**
     * Se obtiene la memoria de la tarjeta grafica
     * @return memoriaTarjetaGrafica
     */
    public double getMemoriaTarjetaGrafica() {
        return memoriaTarjetaGrafica;
    }

    /**
     * Establece la memoria de la tarjeta grafica
     * @param memoriaTarjetaGrafica
     */
    public void setMemoriaTarjetaGrafica(double memoriaTarjetaGrafica) {
        this.memoriaTarjetaGrafica = memoriaTarjetaGrafica;
    }
    
    /**
     * Se obtiene la lista de la descripcion de los componentes 
     * @return una lista de DescripcionComponente
     * @throws BDException
     */
    public ArrayList<DescripcionComponente> getDescComponentes () throws BDException {
        ArrayList<DescripcionComponente> descripcionComponentes = new ArrayList<> ();
        ArrayList<JsonObject> jsonComponentes = GestorPersistenciaConfiguracionPC.getDescComponentesEnConfiguracion(idConfiguracion);
        int idDescripcion;
        String marca;
        String modelo;
        float precio;
        String caracteristicasTecnicas;
        TipoComponente tipo;
        for (JsonObject json: jsonComponentes) {
            idDescripcion = Integer.parseInt(json.getString("idDescripcion"));
            marca = json.getString("marca");
            modelo = json.getString("modelo");
            precio = Float.valueOf(json.getString("precio"));
            caracteristicasTecnicas = json.getString("caracteristicasTecnicas");
            tipo = TipoComponente.getTipo(json.getString("nombreTipoComponente"));
            descripcionComponentes.add(new DescripcionComponente(idDescripcion, marca, modelo, precio, caracteristicasTecnicas, tipo));
        }
        return descripcionComponentes;
    }
    
    /**
     * Se obtiene una lista de PCs en stock
     * @return una lista de PCs
     */
    public PC[] getPCsEnStock() {
        String[] jsonPCs;
        try {
            jsonPCs = GestorPersistenciaPC.getPCsEnStock(this);
            PC[] pcs = new PC[jsonPCs.length];
            for (int i = 0; i < jsonPCs.length; i++) {
                pcs[i] = new PC(jsonPCs[i]);
            }
            return pcs;
        } catch (BDException ex) {
            Logger.getLogger(ConfiguracionPC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
