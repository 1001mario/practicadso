/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import excepciones.BDException;
import datos.GestorPersistenciaEmpleado;
import java.io.StringReader;
import java.sql.Date;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonReaderFactory;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class Empleado extends Usuario {
    
    private Date fechaContratacion;
    private RolEnLaEmpresa rolEmpleado;

    /**
     * Constrctor no vacio de Empleado
     * @param jsonEmpleado
     * @param jsonUsuario
     */
    protected Empleado(String jsonEmpleado, String jsonUsuario) {
        /*Conversion de String a Json*/
        super(jsonUsuario);
        JsonReaderFactory factory = Json.createReaderFactory(null);
        JsonReader jReader= factory.createReader(new StringReader(jsonEmpleado));
        JsonObject jsonObject=jReader.readObject();
        this.fechaContratacion = Date.valueOf(jsonObject.getString("fechaContratacion"));
        this.rolEmpleado = RolEnLaEmpresa.getTipo(jsonObject.getString("rolEmpleado"));
    }
    
    /**
     * Establece el RolEmpleado
     * @param rolEmpleado 
     */
    private void setRolEmpleado (RolEnLaEmpresa rolEmpleado) {
        this.rolEmpleado = rolEmpleado;
    }

    /**
     * Se obtiene el rolEmpleado
     * @return rolEmpleado
     */
    public RolEnLaEmpresa getRolEmpleado(){
        return rolEmpleado;
    }
    /**
     * Establece la fecha de contratacion
     * @param fechaContratacion 
     */
    private void setFechaContratacion(Date fechaContratacion){
        this.fechaContratacion=fechaContratacion;
    }

    /**
     * Se obtiene la fecha de contratacion
     * @return fechaContratacion
     */
    public Date getFechaContratacion(){
        return fechaContratacion;
    }
    
    /**
     * Se obtiene el Empleado
     * @param jsonUsuario
     * @return un empleado
     * @throws BDException
     */
    public static Empleado obtenerEmpleado (String jsonUsuario) throws BDException {
        StringReader strReader=new StringReader(jsonUsuario);
        JsonReader jReader=Json.createReader(strReader);
        JsonObject jsonObject=jReader.readObject();
        String nif = jsonObject.getString("nifCif");
        
        String jsonEmpleado=GestorPersistenciaEmpleado.recuperaEmpleadoPorLogin(nif);
        Empleado empleado = null;
        
        /*Creación del empleado y recuperacion de la persona*/
        if(jsonEmpleado!=null){
            strReader=new StringReader(jsonEmpleado);
            jReader=Json.createReader(strReader);
            jsonObject=jReader.readObject();
            
            empleado = new Empleado(jsonEmpleado,jsonUsuario);
        }
              
        return empleado;
    }
    
}
