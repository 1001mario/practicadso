/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public enum TipoComponente {
    TarjetaGrafica, DiscoDuro, PlacaBase, Caja, Procesador;
    
    /**
     * Trasnforma una cadena correspondiente en el valor de getTipo
     * que le corresponda.
     * @param tipo
     * @return devuelve una valor del tipo Tipo
     */
    public static TipoComponente getTipo (String tipo) {
        switch (tipo) {
            case "TarjetaGrafica": return TarjetaGrafica;
            case "DiscoDuro": return DiscoDuro;
            case "PlacaBase": return PlacaBase;
            case "Caja": return Caja;
            case "Procesador": return Procesador;
            default: return null;
        }
    }
}
