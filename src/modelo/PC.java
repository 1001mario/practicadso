/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import datos.GestorPersistenciaPC;
import excepciones.BDException;
import java.io.IOException;
import java.io.StringReader;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class PC {

    private boolean reservado;
    private int ubicacion, idPedido, etiquetaId;
    private String montador;
    private Date fechaMontaje;
    private ConfiguracionPC configuracion;
    
    /**
     * Se obtiene si esta reservado o no
     * @return reservado
     */
    public boolean getReservado() {
        return reservado;
    }
    /**
     * Establece si esta reservado o no
     * @param reservado 
     */
    private void setReservado(boolean reservado) {
        this.reservado = reservado;
    }

    /**
     * Se obtiene la ubicacion del PC
     * @return ubicacion
     */
    public int getUbicacion() {
        return ubicacion;
    }
    /**
     * Establece la ubicacion de un PC
     * @param ubicacion 
     */
    private void setUbicacion(int ubicacion) {
        this.ubicacion = ubicacion;
    }

    /**
     * Se obtiene el identificador del Pedido PC
     * @return idPedido
     */
    public int getIdPedido() {
        return idPedido;
    }

    /**
     * Se establece el identificador del Pedido PC
     * @param idPedido
     */
    protected void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }
    
    /**
     * Se obtiene la etiquetaID de PC
     * @return etiquetaId
     */
    public int getEtiquetaId(){
        return etiquetaId;
    }
    /**
     * Establece la etiquetaID de PC
     * @param etiquetaId 
     */
    private void setEtiquetaId(int etiquetaId){
        this.etiquetaId = etiquetaId;
    }

    /**
     * Se obtiene la fecha de montaje
     * @return fechaMontaje
     */
    public Date getFechaMontaje() {
        return fechaMontaje;
    }
    /**
     * Establece la fecha de montaje
     * @param fechaMontaje 
     */
    private void setFechaMontaje(Date fechaMontaje) {
        this.fechaMontaje = fechaMontaje;
    }

    /**
     * Se obtiene el montador del PC
     * @return montador
     */
    public String getMontador() {
        return montador;
    }
    /**
     * Se establece el montador del PC
     * @param montador 
     */
    private void setMontador(String montador) {
        this.montador = montador;
    }

    /**
     * Se obtiene la configuracion del PC
     * @return configuracion
     */
    public ConfiguracionPC getConfiguracion() {
        return configuracion;
    }
    /**
     * Se establece la configuracion del PC
     * @param configuracion 
     */
    private void setConfiguracion(ConfiguracionPC configuracion) {
        this.configuracion = configuracion;
    }
    
    /**
     * Para cuando se va a registrar un pc preparandolo en el controlador
     * del cu registrar montaje.
     * @param etiquetaId
     * @param configuracion 
     */
    public PC (int etiquetaId, ConfiguracionPC configuracion) {
        this.etiquetaId = etiquetaId;
        reservado = true;
        fechaMontaje = new Date();
        this.configuracion = configuracion;
    }

    /**
     * Constructor no vacio de PC
     * @param jsonPC
     */
    public PC(String jsonPC) {
        System.out.println(jsonPC);
        JsonObject jsonObject = Json.createReader(new StringReader(jsonPC)).readObject();
        setEtiquetaId(jsonObject.getInt("etiquetaID"));
      
        try {
            DateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.FRANCE);
            setFechaMontaje(format.parse(jsonObject.getString("fechaMontaje")));
        } catch (ParseException ex) {
            Logger.getLogger(PC.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        setMontador(jsonObject.getString("montador"));
        setReservado(jsonObject.getInt("reservado") == 1);
        this.configuracion = ConfiguracionPC.obtenerConfiguracionPCbyID(jsonObject.getInt("idConfiguracion"));
        setUbicacion(jsonObject.getInt("ubicacion"));
        setIdPedido(jsonObject.getInt("idPedido"));
    }
    
    /**
     * Se reserva un PC mediante su etiquetaID
     */
    public void reservar() {
        if(!reservado){
            reservado = true;
            GestorPersistenciaPC.reservarPC(etiquetaId);
        } else throw new IllegalStateException();
    }
    
    /**
     * Se obtiene la etiquetaId de un PC
     * @param idPC
     * @return pc
     * @throws SQLException
     * @throws IOException
     */
    public static PC obtenerEtiquetaID(int idPC) throws SQLException, IOException {
      
        String jsonPC = GestorPersistenciaPC.getPCByEtiquetaID(idPC);
        PC pc = null;
        
        if(jsonPC != null){
            pc = new PC(jsonPC);
        }
        return pc;
    }
    
    /**
     * se comprueba si la etiquetaID es valida
     * @param idPC
     * @return si la etiquetaID es valida o no
     * @throws BDException
     */
    public static boolean comprobarIDValido (int idPC) throws BDException {
        return GestorPersistenciaPC.idValido(idPC);
    }
    
    /**
     * Se registra un PC en la BD
     * @throws BDException
     */
    public void registrarPC () throws BDException {
        GestorPersistenciaPC.registrarPC(this);
    }
}
