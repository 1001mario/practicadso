/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public enum RolEnLaEmpresa {
    tecnicoTaller,
    gerenteVentas,
    encargadoAlmacen;
    /**
     * Se obtiene el tipo de empleado de RolEnLaEmpresa
     * @param rolEmpleado
     * @return un rol de empleado
     */
    public static RolEnLaEmpresa getTipo(String rolEmpleado) {
        switch(rolEmpleado){
          case "1": return encargadoAlmacen;
          case "2": return tecnicoTaller;
          case "3": return gerenteVentas;
          default: return null;
        }
    }
}
