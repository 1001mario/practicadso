/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import datos.GestorPersistenciaComponente;
import excepciones.BDException;
import excepciones.ComponenteException;
import excepciones.IDException;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonObject;
/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class Componente {
    
    private int etiquetaId;
    private boolean reservado;
    private Date fechaEntrada;
    private DescripcionComponente descripcionComponente;
    private PC pc;
    private EspacioAlmacenamiento espacio;
    /**
     * Constructor de Componente
     * @param etiquetaId
     * @param reservado
     * @param fechaEntrada
     * @param descripcionComponente
     * @param espacio 
     */
    public Componente(int etiquetaId, boolean reservado, Date fechaEntrada, DescripcionComponente descripcionComponente, EspacioAlmacenamiento espacio) {
        this.etiquetaId = etiquetaId;
        this.reservado = reservado;
        this.fechaEntrada = fechaEntrada;
        this.descripcionComponente = descripcionComponente;
        this.espacio = espacio;
    }
    /**
     * Se obtiene un componente by idcomponente
     * @param idComponente
     * @return el componente con el idcomponente indicado
     * @throws BDException
     * @throws IDException
     * @throws ComponenteException 
     */
    public static Componente obtenerComponentebyID (int idComponente) throws BDException, IDException, ComponenteException {
        if(!GestorPersistenciaComponente.idValido(idComponente)) {
            throw new IDException("La etiqueta identificativa no existe");
        }
        JsonObject jsonComponente = GestorPersistenciaComponente.getComponenteReservado(idComponente);
        if (jsonComponente == null) {
            throw new ComponenteException("La etiqueta identificativa pertenece a un componente ya reservado");
        }
        int etiquetaId = Integer.parseInt(jsonComponente.getString("etiquetaId"));
        boolean reservado = Integer.parseInt(jsonComponente.getString("reservado"))==1;
        int idDescripcion = Integer.parseInt(jsonComponente.getString("idDescripcion"));
        TipoComponente tipo = TipoComponente.getTipo(jsonComponente.getString("tipo"));
        String marca = jsonComponente.getString("marca");
        String modelo = jsonComponente.getString("modelo");
        String caracteristicasTecnicas = jsonComponente.getString("caracteristicasTecnicas");
        float precio = jsonComponente.getInt("precio");
        int idUbicacion = jsonComponente.getInt("ubicacion");
        int seccion = jsonComponente.getInt("seccion");
        int zona = jsonComponente.getInt("zona");
        int estanteria = jsonComponente.getInt("estanteria");
        int ocupado = jsonComponente.getInt("ocupado");
        Date fechaEntrada = Date.valueOf(jsonComponente.getString("fechaEntrada"));
        DescripcionComponente descripcionComponente = new DescripcionComponente(idDescripcion, marca, modelo, precio, caracteristicasTecnicas, tipo);
        EspacioAlmacenamiento espacio = new EspacioAlmacenamiento(idUbicacion, seccion, zona, estanteria, ocupado);
        return new Componente(etiquetaId, reservado, fechaEntrada, descripcionComponente, espacio);
    }
    /**
     * Devuelve la descipcion de un componente
     * @return 
     */
    public DescripcionComponente getDescripcionComponente () {
        return descripcionComponente;
    }
    /**
     * Se asocia un Pc con su etiquetaID
     * @param pc
     * @throws BDException 
     */
    public void asociarPC(PC pc) throws BDException {
        this.pc = pc;
        GestorPersistenciaComponente.actualizarPC(etiquetaId, pc.getEtiquetaId());
    }
    /**
     * Se reserva un componente mediante etiquetaID
     */
    public void reservar() {
        this.reservado = true;
        try {
            GestorPersistenciaComponente.reservar(etiquetaId);
        } catch (BDException ex) {
            Logger.getLogger(Componente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Se obtiene la etiquetaID
     * @return etiquetaID
     */
    public int getEtiquetaId() {
        return etiquetaId;
    }

    /**
     * Establece la etiquetaID
     * @param etiquetaId
     */
    public void setEtiquetaId(int etiquetaId) {
        this.etiquetaId = etiquetaId;
    }

    /**
     * Se obtiene si es reservado o no
     * @return
     */
    public boolean isReservado() {
        return reservado;
    }

    /**
     * Establece si esta reservado o no
     * @param reservado
     */
    public void setReservado(boolean reservado) {
        this.reservado = reservado;
    }

    /**
     * Se obtiene la fecha de entrada
     * @return la fecha de entrada
     */
    public Date getFechaEntrada() {
        return fechaEntrada;
    }

    /**
     * Establece la fecha de entrada
     * @param fechaEntrada
     */
    public void setFechaEntrada(Date fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    /**
     * Se obtiene el PC
     * @return Pc
     */
    public PC getPc() {
        return pc;
    }

    /**
     * Establece el PC
     * @param pc
     */
    public void setPc(PC pc) {
        this.pc = pc;
    }

    /**
     * Se obtiene el espacio de almacenamiento 
     * @return el espacio de almacenamento
     */
    public EspacioAlmacenamiento getEspacio() {
        return espacio;
    }

    /**
     * Establece el espacio de almacenamiento
     * @param espacio
     */
    public void setEspacio(EspacioAlmacenamiento espacio) {
        this.espacio = espacio;
    }
    
}
