/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class Empresa extends Usuario {

    /**
     * Constructor no vacio de Empresa
     * @param jsonUsuario 
     */
    public Empresa(String jsonUsuario) {
        super(jsonUsuario);
    }

}
