/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import excepciones.BDException;
import excepciones.PCNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ControladorCUAsignarUbicacionPC {
    
    private static ControladorCUAsignarUbicacionPC controladorCU;
    private EspacioAlmacenamiento espacioAlmacenamiento;
    
    /**
     * Funcion de singleton del controlador del CUAsignarUbicacionPC
     * @return controladorCU
     */
    public static ControladorCUAsignarUbicacionPC getControlador() {
        if (controladorCU == null) {
            controladorCU = new ControladorCUAsignarUbicacionPC();
        }
        return controladorCU;
    }
       
    /**
     * Se comprueba la etiquetaID de un PC
     * @param idPC
     * @return pc
     * @throws PCNotFoundException
     * @throws SQLException
     * @throws IOException
     */
    public static PC comprobarEtiquetaID(int idPC)throws PCNotFoundException, SQLException, IOException {
        PC pc;   
            // Obtenemos el pc, si no existe, devuelve null y gestionamos el error
            try { 
                pc = PC.obtenerEtiquetaID(idPC);
                if(pc==null){
                    throw new PCNotFoundException("El número de idPC no es correcto");
                }
            } catch (PCNotFoundException ex) {
                throw new PCNotFoundException("idPC no Encontrado: "+ex.getMessage());
            }
        // Devolvemos el pc
        return pc;
    }

    /**
     * Se guarda el espacio de almacenamiento elegido
     * @param espacioAlmacenamiento
     */
    public void guardarEspacioElegido(EspacioAlmacenamiento espacioAlmacenamiento) {
        this.espacioAlmacenamiento=espacioAlmacenamiento;
    }
    
    /**
     * Se obtiene los espacio de almacenamiento que no estan ocupados
     * @return espacios de almacenamiento no ocupados
     * @throws BDException
     */
    public EspacioAlmacenamiento[] obtenerEspaciosLibres () throws BDException {
        return EspacioAlmacenamiento.getEspacios();
    }
    
}
