/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import excepciones.BDException;
import excepciones.ComponenteException;
import excepciones.IDException;
import java.util.ArrayList;

/**
 * Sigue el patron singleton porque solamente se puede realizar
 * este caso de uso una vez por ordenador independientemente de las
 * ejecuciones de la aplicacion, de manera que con una sola instancia
 * nos viene perfecto para atender la solicitud del tecnico.
 * 
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ControladorCURegistrarMontajePC {
    private static ControladorCURegistrarMontajePC controladorCU;
    private PC pc;
    private PedidoPC pedido;
    private ArrayList<DescripcionComponente> descripcionesComponentes;
    private ArrayList<Componente> componentes;
    
    /**
     * Constructor vacio de ControladorCURegistrarMontajePC
     */
    public ControladorCURegistrarMontajePC () {
        descripcionesComponentes = new ArrayList<> ();
        componentes = new ArrayList<> ();
    }
    
    /**
     * Obtiene la instancia especifica del controlador del CU identificarse.
     * 
     * @return ControladorCUIdentificarse
     */
    public static ControladorCURegistrarMontajePC getControlador() {
        if (controladorCU == null) {
            controladorCU = new ControladorCURegistrarMontajePC();
        }
        return controladorCU;
    }
    
    /**
     * Se obtiene los pedidos en estado EnProceso
     * @return lista de pedidos de PedidoPC
     * @throws BDException
     */
    public PedidoPC[] obtenerPedidosEnProceso () throws BDException {
        return PedidoPC.getPedidos(EstadoVentaPCs.EnProceso);
    }
    
    /**
     * se guarda el pedido elegido 
     * @param pedido
     */
    public void guardarPedidoElegido (PedidoPC pedido) {
        this.pedido = pedido;
    }
    
    /**
     * Se comprueba la etiquetaID de un PC
     * @param idPC
     * @throws BDException
     * @throws IDException
     */
    public void comprobarEtiquetaPC (int idPC) throws BDException, IDException {
        boolean idValido = PC.comprobarIDValido(idPC);
        if (idValido) {
            // Preparar pc y almacenarlo para despues guardarlo en la bd!
            pc = new PC(idPC, pedido.getConfiguracion());
            descripcionesComponentes = pedido.getConfiguracion().getDescComponentes();
        } else {
            // Informar de idExistente en bd y pedir otro
            throw new IDException("La etiqueta identificativa ya existe");
        }
    }
    
    /**
     * Se comprueba idcomponente de un Componente
     * @param idComponente
     * @throws IDException
     * @throws BDException
     * @throws ComponenteException
     */
    public void comprobarEtiquetaComponente (int idComponente) throws IDException, BDException, ComponenteException {
        Componente componente = Componente.obtenerComponentebyID(idComponente);
        DescripcionComponente descripcion = componente.getDescripcionComponente();
        if (descripcionesComponentes.contains(descripcion)) {
            descripcionesComponentes.remove(descripcion);
            componentes.add(componente);
        } else {
            throw new ComponenteException("La etiqueta identificativa no pertenece a un componente descrito en la configuracion");
        }
    }
    
    /**
     * Se obtiene los componentes restantes
     * @return
     */
    public int getComponentesRestantes () {
        return descripcionesComponentes.size();
    }
    
    /**
     * Se registra un PC en la DB
     * @throws BDException
     */
    public void registraPC () throws BDException {
        pc.setIdPedido(pedido.getIdPedido());
        pc.registrarPC();
        for (Componente componente: componentes) {
            componente.asociarPC(pc);
        }
    }
}
