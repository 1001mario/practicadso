/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import excepciones.BDException;
import excepciones.ComponenteException;
import excepciones.IDException;
import modelo.ControladorCURegistrarMontajePC;
import modelo.MaquinaEstados;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ControladorVistaSolicitarComponente {
    private VistaSolicitarComponente vista;
    
    public ControladorVistaSolicitarComponente (VistaSolicitarComponente vista) {
        this.vista = vista;
    }
    
    public void cancelar() {
        MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
        maquina.volverMenu();
    }
    
    public void verificarComponenteValido () {
        ControladorCURegistrarMontajePC controladorCU;
        try {
            controladorCU = ControladorCURegistrarMontajePC.getControlador();
            controladorCU.comprobarEtiquetaComponente(vista.getEtiquetaComponente());
            int componentesFaltantes = controladorCU.getComponentesRestantes();
            MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
            if (componentesFaltantes == 0) {
                // Registramos datos de pc y componentes asociados a ese pc con controlador de cu.
                controladorCU.registraPC();
                maquina.setEstado(new VistaPCGuardado());
            } else {
                maquina.setEstado(new VistaSolicitarComponente());
            }
            maquina.actualiza();
        } catch (IDException | ComponenteException e) {
            vista.mensajeError(e.getMessage());
        } catch (BDException e) {
            e.printStackTrace();
        }
    }
}
