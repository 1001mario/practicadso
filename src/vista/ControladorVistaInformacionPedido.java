/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import modelo.ControladorCUProcesarPedidos;
import modelo.PedidoPC;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ControladorVistaInformacionPedido {
    VistaInformacionPedido vista;

    public ControladorVistaInformacionPedido(VistaInformacionPedido vista) {
        this.vista = vista;
    }
    
    /**
     * Procede con la reserva de PCs para un pedido
     * @param pedido 
     */
    public void reservarStock(PedidoPC pedido) {
       int stock = ControladorCUProcesarPedidos.getControlador().reservarPCsStock(pedido);
       vista.informaReserva(stock);
    }

    public void solicitarComponentes(PedidoPC pedido) {
       ControladorCUProcesarPedidos.getControlador().reservarComponentes(pedido);
    }
}
