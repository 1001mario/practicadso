/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import modelo.ControladorCURegistrarMontajePC;
import modelo.MaquinaEstados;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
class ControladorVistaPedidosEnProceso {
    private VistaPedidosEnProceso vista;
    
    public ControladorVistaPedidosEnProceso (VistaPedidosEnProceso vista) {
        this.vista = vista;
    }
    
    public void cancelar () {
        MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
        maquina.volverMenu();
    }
    
    public void seleccionarPedido () {
        // Guardamos en el controlador de cu el pedido elegido con la configuracion.
        ControladorCURegistrarMontajePC.getControlador().guardarPedidoElegido(vista.getPedidoElegido());
        // y solicitamos el id del nuevo pc
        MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
        maquina.setEstado(new VistaSolicitarIdPC());
        maquina.actualiza();
    }
}
