/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import excepciones.PedidosNotFoundException;
import javax.swing.JFrame;
import modelo.ControladorCUProcesarPedidos;
import modelo.MaquinaEstados;
import modelo.PedidoPC;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ControladorVistaOpcionesGerenteVentas {
    private VistaOpcionesGerenteVentas vista;
    
    public ControladorVistaOpcionesGerenteVentas (VistaOpcionesGerenteVentas vista) {
        this.vista = vista;
    }
    
    /**
     * Obtiene y muestra la lista de pedidos solicitados
     */
    public void obtenerListaPedidos () {
        JFrame vistaPedidos;
        try {
            PedidoPC[] pedidos = ControladorCUProcesarPedidos.getControlador().getListaPedidos();
            if(pedidos!=null) {  
                vistaPedidos = new VistaProcesarPedido(pedidos);
                MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
                maquina.setEstado(vistaPedidos);
                maquina.actualiza();
            } else vista.informarAusenciaDePedidos();
        } catch (PedidosNotFoundException e) {
            e.printStackTrace();
        }
    }
}
