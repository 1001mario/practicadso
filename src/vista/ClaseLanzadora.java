/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import modelo.MaquinaEstados;

/**
 * Clase lanzadora del sistema.
 * 
 * Comienza la maquina de estados con la vista de identificarse para poder
 * realizar el resto de los casos de uso, es la unica clase que permite una ejecucion
 * por parte del proyecto, el resto de vistas no son ejecutables por si solas.
 * 
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ClaseLanzadora {
    public static void main (String args[]) {
        // Inicializa la maquina de estados mediante el caso de uso identificarse
        MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
        maquina.actualiza();
    }
}
