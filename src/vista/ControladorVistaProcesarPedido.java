/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import javax.swing.JFrame;
import modelo.MaquinaEstados;
import modelo.PedidoPC;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ControladorVistaProcesarPedido {
    private VistaProcesarPedido vista;
    
    public ControladorVistaProcesarPedido(VistaProcesarPedido vista){
        this.vista=vista;
    }
    
    public void seleccionarPedido () {
        PedidoPC pedidoSeleccionado = vista.getPedidoSeleccionado();
        JFrame vistaInformacion = new VistaInformacionPedido(pedidoSeleccionado);
        MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
        maquina.setEstado(vistaInformacion);
        maquina.actualiza();
    }
}
