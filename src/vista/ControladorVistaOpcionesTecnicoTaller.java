/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import excepciones.BDException;
import modelo.ControladorCURegistrarMontajePC;
import modelo.MaquinaEstados;
import modelo.PedidoPC;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
class ControladorVistaOpcionesTecnicoTaller {
    private VistaOpcionesTecnicoTaller vista;
    
    public ControladorVistaOpcionesTecnicoTaller (VistaOpcionesTecnicoTaller vista) {
        this.vista = vista;
    }
    
    public void salir () {
        System.exit(0);
    }
    
    /**
     * Obtiene pedidos en proceso y su configuracion.
     */
    public void getPedidosEnProceso () {
        try {
            PedidoPC[] pedidos = ControladorCURegistrarMontajePC.getControlador().obtenerPedidosEnProceso();
            MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
            maquina.setEstado(new VistaPedidosEnProceso(pedidos));
            maquina.actualiza();
        } catch (BDException e) {
            e.printStackTrace();
        }
    }
}
