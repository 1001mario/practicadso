/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import excepciones.PCNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.ControladorCUAsignarUbicacionPC;
import modelo.EspacioAlmacenamiento;
import modelo.MaquinaEstados;
import modelo.PC;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ControladorVistaAsignarUbicacionPC {
    private VistaAsignarUbicacionPC vista;
    private PC pc;

    public ControladorVistaAsignarUbicacionPC(VistaAsignarUbicacionPC vista) {
        this.vista = vista;
    }
    
    /**
     * Comprueba la etiqueta identificativa de un pc y muestra los espacios no ocupados
     * @throws PCNotFoundException
     * @throws IOException 
     */
    protected void comprobarIdPC() throws PCNotFoundException, IOException{
        try {
            int idPC = Integer.parseInt(vista.getEtiquetaID());
            pc = ControladorCUAsignarUbicacionPC.comprobarEtiquetaID(idPC);
            EspacioAlmacenamiento[] espacios = ControladorCUAsignarUbicacionPC.getControlador().obtenerEspaciosLibres();
            MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
            maquina.setEstado(new VistaInfoEspacioPC(espacios));
            maquina.actualiza();
        } catch (SQLException ex) {
            Logger.getLogger(ControladorVistaAsignarUbicacionPC.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
