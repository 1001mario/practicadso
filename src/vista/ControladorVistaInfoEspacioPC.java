/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import modelo.ControladorCUAsignarUbicacionPC;
import modelo.MaquinaEstados;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ControladorVistaInfoEspacioPC {
    private VistaInfoEspacioPC vista;

    public ControladorVistaInfoEspacioPC(VistaInfoEspacioPC vista) {
        this.vista = vista;
    }
    
    /**
     * Permite elegir un espacio de almacenamiento y mostrarlo
     */
    public void seleccionarEspacio(){
        
        ControladorCUAsignarUbicacionPC.getControlador().guardarEspacioElegido(vista.getEspacioElegido());
        MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
        maquina.setEstado(new VistaPCGuardado());
        maquina.actualiza();
    }
    
    public void cancelar () {
        MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
        maquina.volverMenu();
    }
}
