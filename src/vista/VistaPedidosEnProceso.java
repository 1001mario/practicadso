/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.awt.Color;
import javax.swing.table.DefaultTableModel;
import modelo.PedidoPC;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class VistaPedidosEnProceso extends javax.swing.JFrame {
    private ControladorVistaPedidosEnProceso controlador;
    private PedidoPC[] pedidos;
    
    /**
     * Creates new form VistaPedidosEnProceso
     * @param pedidos
     */
    public VistaPedidosEnProceso(PedidoPC[] pedidos) {
        initComponents();
        controlador = new ControladorVistaPedidosEnProceso (this);
        this.pedidos = pedidos;
        setPedidos();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        botonSeleccionar = new javax.swing.JButton();
        botonCancelar = new javax.swing.JButton();
        mensajeError = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "idPedido", "idConfiguracion"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        botonSeleccionar.setText("Seleccionar");
        botonSeleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSeleccionarActionPerformed(evt);
            }
        });

        botonCancelar.setText("Cancelar");
        botonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(botonCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botonSeleccionar))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mensajeError))
                        .addGap(0, 7, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(mensajeError)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonSeleccionar)
                    .addComponent(botonCancelar))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void botonSeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSeleccionarActionPerformed
        // TODO add your handling code here:
        if (jTable1.getSelectedRow() != -1) {
            // Se ha seleccionado un pedido con la configuracion
            controlador.seleccionarPedido();
        } else {
            // No se ha seleccionado ningun pedido
            mensajeError();
        }
    }//GEN-LAST:event_botonSeleccionarActionPerformed

    private void botonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCancelarActionPerformed
        // TODO add your handling code here:
        controlador.cancelar();
    }//GEN-LAST:event_botonCancelarActionPerformed
    
    private void setPedidos () {
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (pedidos.length != 0) {
            // Solamente actualizamos cuando existen pedidos por mostrar!

            for (int i = 0; i < pedidos.length; i++) {
                Object[] row = new Object[2];
                row[0] = pedidos[i].getIdPedido();
                row[1] = pedidos[i].getConfiguracion().getIdConfiguracion();
                model.addRow(row);
            }
        }
    }
    
    public void mensajeError () {
        mensajeError.setForeground(Color.red);
        mensajeError.setText("Debe seleccionar un pedido!");
    }
    
    public PedidoPC getPedidoElegido () {
        return pedidos[jTable1.getSelectedRow()];
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonCancelar;
    private javax.swing.JButton botonSeleccionar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel mensajeError;
    // End of variables declaration//GEN-END:variables
}
