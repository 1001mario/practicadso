/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;
import excepciones.EmpNotFoundException;
import java.io.IOException;
import javax.swing.JFrame;
import modelo.ControladorCUIdentificarse;
import modelo.MaquinaEstados;
import modelo.RolEnLaEmpresa;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ControladorVistaLogin {
    private VistaLogin vista;
    
    public ControladorVistaLogin (VistaLogin vista) {
        this.vista = vista;
    }
    
    /**
     * Comprobamos que el usuario y su password existe y el tipo de empleado
     * @throws java.lang.ClassNotFoundException
     * @throws java.io.IOException
     */
    protected void procesaIdentificacion() throws ClassNotFoundException, IOException{
        /*Traemos el contenido de los elementos*/
        String login = vista.getLogin();
        String password = vista.getPassword();
        RolEnLaEmpresa rolEmpleado;
        JFrame v = null;
        
        try{
            rolEmpleado = ControladorCUIdentificarse.getControlador().identificarse(login, password);
            switch(rolEmpleado){
                case tecnicoTaller:
                    v=new VistaOpcionesTecnicoTaller();
                    break;
                case gerenteVentas:
                    v=new VistaOpcionesGerenteVentas();
                    break;
                case encargadoAlmacen:
                    v=new VistaOpcionesEncargadoAlmacen();
                    break;
            }
            vista.dispose();
            MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
            maquina.setEstado(v);
            maquina.actualiza();
        }catch(EmpNotFoundException ex){
            vista.lanzaError(ex.getMessage());
        }
    }
    
    public void salir () {
        System.exit(0);
    }
}
