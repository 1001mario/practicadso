/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import excepciones.BDException;
import excepciones.IDException;
import modelo.ControladorCURegistrarMontajePC;
import modelo.MaquinaEstados;

/**
 *
 * @author Nuria Cancho Diaz
 * @author Mario Benito Rodriguez
 * @author Jairo Velasco Martin
 */
public class ControladorVistaSolicitarIdPC {
    private VistaSolicitarIdPC vista;
    
    public ControladorVistaSolicitarIdPC (VistaSolicitarIdPC vista) {
        this.vista = vista;
    }
    
    public void introducirID () {
        // Se debe comprobar si el id del PC no existe en la bd.
        try {
            ControladorCURegistrarMontajePC.getControlador().comprobarEtiquetaPC(vista.getEtiquetaPC());
            MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
            maquina.setEstado(new VistaSolicitarComponente());
            maquina.actualiza();
        } catch (BDException e) {
            e.printStackTrace();
        } catch (IDException e) {
            vista.mensajeError(e.getMessage());
        }
    }
    
    public void cancelar () {
        MaquinaEstados maquina = MaquinaEstados.getMaquinaEstados();
        maquina.volverMenu();
    }
}
